﻿#include <iostream>
#include <chrono>
#define N 100

class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;
    std::chrono::time_point<clock_t> m_beg;
public:
    Timer() : m_beg(clock_t::now())
    {}
    void reset()
    {
        m_beg = clock_t::now();
    }
    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};

bool Equal (int m)
{
    for (int d = 2; d <= sqrt(m); d++)   
        if (m % d == 0)
            return false;
    return true;
}


int main()
{
    srand(time(0));
    int m;
    int mas[N];
    for (int i = 0; i < N; i++) {
        mas[i] = rand();
        std::cout << mas[i] << " " << std::endl;
    }
    Timer t;
    int k = 0;
    for (int i = 0; i < N; i++) {  //n операций
        m = mas[i];                //1 операция
        if (Equal(m) == true)      //1 операция
            k += 1;                //1 операция
    }
    std::cout << k << std::endl;
    std::cout << "Time taken: " << t.elapsed() << '\n';
}

//Лучший случай - тета(n)
//Средний случай - тета(n)
//Худший случай - тета(n)
//Расчётное время: 3*10^-4 сек
//Среднее время: 4*10^-4 сек